init: ## deploy dotfiles
	ln -vsf ${PWD}/alacritty/.config/alacritty ${HOME}/.config/alacritty
	ln -vsf ${PWD}/bash_aliases/.bash_aliases ${HOME}/.bash_aliases
	ln -vsf ${PWD}/readline/.inputrc ${HOME}/.inputrc
	ln -vsf ${PWD}/bash/.bashrc ${HOME}/.bashrc
	ln -vsf ${PWD}/vim/.vim/UltiSnips ${HOME}/.vim/UltiSnips
	ln -vsf ${PWD}/vim/.vim/vimrc ${HOME}/.vim/vimrc
	ln -vsf ${PWD}/tmux/.tmux.conf ${HOME}/.tmux.conf
	ln -vsf ${PWD}/dwm/config.def.h ${HOME}/.dwm/config.def.h
	ln -vsf ${PWD}/dwm/dwm.c ${HOME}/.dwm/dwm.c
	ln -vsf ${PWD}/dwm/autostart.sh ${HOME}/.dwm/autostart.sh
	ln -vsf ${PWD}/dwmblocks ${HOME}/.dwmblocks
	ln -vsf ${PWD}/xinit/.xinitrc ${HOME}/.xinitrc
	ln -vsf ${PWD}/gtk/settings.ini ${HOME}/.config/gtk-3.0/settings.ini
	ln -vsf ${PWD}/gtk/.gtkrc-2.0 ${HOME}/.gtkrc-2.0
	vim -c "PlugInstall"
	vim -c "call mkdp#util#install()"

install: ## install all dependencies
	sudo pacman -S --noconfirm alacritty gvim tmux feh picom\
		xorg-server xorg-apps xdg-user-dirs iproute2 networkmanager wireless_tools\
		wpa_suplicant man-db alsa-utils alsa-plugins alsa-firmware pulseaudio-alsa pamixer\
		vlc scrot mupdf noto-fonts-emoji arc-gtk-theme
	curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
		https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	git clone https://git.suckless.org/dwm ${HOME}/.dwm

config:
	amixer sset Master unmute
	amixer sset Speaker unmute
	amixer sset Headphone unmute
	cp ${HOME}/.dwm/config.def.h ${HOME}/.dwm/config.h
	cd ${HOME}/.dwm/
	make && sudo make clean install
	cd ${HOME}/.dwmblocks/
	sudo make clean install

font:
	mkdir -p ${HOME}/.local/share/fonts
	curl -sfLo ${HOME}/.local/share/fonts/"Ubuntu Mono Nerd Font Complete.ttf"\
		https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/UbuntuMono/Regular/complete/Ubuntu%20Mono%20Nerd%20Font%20Complete.ttf
	curl -sfLo ${HOME}/.local/share/fonts/"Ubuntu Mono Nerd Font Complete Mono.ttf" \
		https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/UbuntuMono/Regular/complete/Ubuntu%20Mono%20Nerd%20Font%20Complete%20Mono.ttf
	curl -sfLo ${HOME}/.local/share/fonts/"Ubuntu Mono Italic Nerd Font Complete.ttf" \
		https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/UbuntuMono/Regular-Italic/complete/Ubuntu%20Mono%20Italic%20Nerd%20Font%20Complete.ttf
	curl -sfLo ${HOME}/.local/share/fonts/"Ubuntu Mono Italic Nerd Font Complete Mono.ttf" \
		https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/UbuntuMono/Regular-Italic/complete/Ubuntu%20Mono%20Italic%20Nerd%20Font%20Complete%20Mono.ttf
	curl -sfLo ${HOME}/.local/share/fonts/"Ubuntu Mono Bold Nerd Font Complete.ttf" \
		https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/UbuntuMono/Bold/complete/Ubuntu%20Mono%20Bold%20Nerd%20Font%20Complete.ttf
	curl -sfLo ${HOME}/.local/share/fonts/"Ubuntu Mono Bold Nerd Font Complete Mono.ttf" \
		https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/UbuntuMono/Bold/complete/Ubuntu%20Mono%20Bold%20Nerd%20Font%20Complete%20Mono.ttf
	curl -sfLo ${HOME}/.local/share/fonts/"Ubuntu Mono Bold Italic Nerd Font Complete.ttf" \
		https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/UbuntuMono/Bold-Italic/complete/Ubuntu%20Mono%20Bold%20Italic%20Nerd%20Font%20Complete.ttf
	curl -sfLo ${HOME}/.local/share/fonts/"Ubuntu Mono Bold Italic Nerd Font Complete Mono.ttf" \
		https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/UbuntuMono/Bold-Italic/complete/Ubuntu%20Mono%20Bold%20Italic%20Nerd%20Font%20Complete%20Mono.ttf
	fc-cache ${HOME}/.local/share/fonts/
