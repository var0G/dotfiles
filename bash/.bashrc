#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

HISTCONTROL=ignoreboth

#append to the history file
shopt -s histappend

HISTSIZE=1000
HISTFILESIZE=2000

#cd into directory just typing the name of the dir.
shopt -s autocd
#update the values of lines and columns.
shopt -s checkwinsize


#function for git branches
git_branch_name()
{
  branch=$(git symbolic-ref HEAD 2> /dev/null | awk 'BEGIN{FS="/"} {print $NF}')
  if [[ $branch == "" ]];
  then
    :
  else
    echo ''$branch
  fi
}

case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	color_prompt=yes
    else
	color_prompt=
    fi
fi

alias ls='ls --color=auto'

cyan_bold='\[\033[01;36m\]'
green_bold='\[\033[01;32m\]'
red_bold='\[\033[01;31m\]'
blue='\[\033[0;34m\]'
yellow='\[\033[01;33m\]'
reset='\[\033[00m\]'

if [ "$color_prompt" = yes ]; then
    PS1="$cyan_bold\u$blue $cyan_bold\h $red_bold\W$blue\$(git_branch_name)$yellow $reset "
else
    PS1='\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

#Paths
if [ -f ~/.bash_aliases ]; then
. ~/.bash_aliases
fi

export EDITOR=vim

if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    elif [ -f /usr/share/bash-completion/completion ]; then
        . /usr/share/bash-completion/completion
    fi
fi

if [ -d "$HOME/bin" ]; then
    PATH="$HOME/bin:$PATH"
fi
PATH="$HOME/.local/bin/:$PATH"
