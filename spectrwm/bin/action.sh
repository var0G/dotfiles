#!/bin/bash

SLEEP_SEC=5
PATH_BATT="/sys/class/power_supply/BATT/" 
SEPR="|"

Battery_info(){

    battery_charg=$(cat $PATH_BATT/capacity)
    battery_status=$(cat $PATH_BATT/status)
    battery_status="$(echo "$battery_status" | sed 's/Charging/↑/;s/Discharging/↓/;s/Full//;s/Unknown//')" 
    echo -e "Battery: $battery_status$battery_charg%"
}

Volume_info(){

    volume=$(pamixer --get-volume)
    volume_mute=$(pamixer --get-mute)

    if [ $volume_mute == false ]; then
        echo -e "Volume: $volume%"
    else
        echo -e "Volume: $volume_mute"
    fi
}

Net_info(){
    network=$(nmcli networking connectivity)
    echo -e "Network: $network"
}

Date_info(){
    date=$(date +%a-%b-%d-%Y-%R)
    echo -e "$date"
}


while :; do

	#spectrwm bar_print can't handle UTF-8 characters, such as degree symbol
	#echo -e "$POWER_STR  $TEMP_STR  $CPUFREQ_STR  $CPULOAD_STR  $MEM_STR  $WLAN_STR"
        #alternatively if you prefer a different date format
        echo -e "+@fg=2;$(Date_info) $SEPR+@fg=2; +@fg=1;$(Battery_info)+@fg=1; $SEPR" \
            +"+@fg=4;$(Volume_info)+@fg=4; $SEPR +@fg=3;$(Net_info)+@fg=3;"
	sleep $SLEEP_SEC
done
