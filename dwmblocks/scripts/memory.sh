#!/bin/bash

RAM_MEMORY=$(free -h | awk '/^Mem/ {print $3"/"$2}' | sed s/i//g)
echo "$RAM_MEMORY"
