#!/bin/bash

if [ -d "/sys/class/power_supply/BATT/" ]; then
    PATH_BATT="/sys/class/power_supply/BATT/" 
    battery_charg=$(cat $PATH_BATT/capacity)
    battery_status=$(cat $PATH_BATT/status)
    battery_status="$(echo "$battery_status" | sed 's/Charging/↑/;s/Discharging/↓/;s/Full//;s/Unknown//')" 
    echo -e "  $battery_status$battery_charg%"
fi
