//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{" \uf6ff ", "$HOME/.dwmblocks/scripts/ethernet.sh",	5,		1},
	{" \uf85a ", "$HOME/.dwmblocks/scripts/memory.sh",	30,		3},
	{" \ufa7d ", "$HOME/.dwmblocks/scripts/volume.sh",	0,		10},
	{" \uf073 ", "$HOME/.dwmblocks/scripts/date.sh",					60,		4},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ";
static unsigned int delimLen = 5;
