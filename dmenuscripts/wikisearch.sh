#!/bin/bash

name=$(echo | dmenu -p "Search on Arch-wiki offline:" $@)

search=$(wiki-search $name | dmenu -l 20 -p "Select the subject: " | awk '{print $1}') 

wiki-search-html $search > /dev/null 2>&1 & 
