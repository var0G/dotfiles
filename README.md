# Dotfiles 

My dotfiles and workflow on Arch. 

<img src="screen.png">

## (under construction).

## Set up

work on Arch linux  

* ```make install``` Install de dependencies.
* ```make init``` Deploy the symbolic links.
* ```make config```  Unmute the sound, configure the window manager and the status bar.
* (optional but recommended) ```make font```, download and store in cache Ubuntu
  Mono nerd font.

## Miscellaneous

* Swap caps for esc ``dconf write
  /org/gnome/desktop/input-sources/xkb-options"['caps:swapescape']"`` or  
  ``setxkbmap us altgr-intl -option caps:swapescape``

## Vimrc plugins

<details>
<summary><strong>Show plugins</strong></summary>

* The most important: [gruvbox-community/gruvbox](http://github.com/gruvbox-community/gruvbox.git)

* [SirVer/ultisnips](https://github.com/SirVer/ultisnips.git)

* [lervag/vimtex](https://github.com/lervag/vimtex.git)

* [itchyny/lightline.vim](https://github.com/itchyny/lightline.vim.git)

* [ap/vim-css-color](https://github.com/ap/vim-css-color.git)

* [Mathjax](https://github.com/iamcco/mathjax-support-for-mkdp)

* [Tabular](https://github.com/godlygeek/tabular)

* [iamcco/markdown-preview.nvim](https://github.com/iamcco/markdown-preview.nvim.git)

* [godlygeek/tabular](https://github.com/godlygeek/tabular)
 
</details>

## Links

* Plugin Manager: [Vim-plug](https://github.com/junegunn/vim-plug)

* Terminal emulator: [Alacritty](https://github.com/alacritty/alacritty) 

## Thanks
*  Special thanks to [Awerito](https://gitlab.com/awerito) for inspire this
   dotfile.
