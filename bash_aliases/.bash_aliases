#Aliase
# alias alias_name="command_to_run"

# list normal and hiden file
alias ll='ls -lah'
#Open rc file quickly
alias vimrc='vim ~/.vim/vimrc'
#Open tmux with new session name
alias tmxn='tmux new -s'
#Alias for git
alias gs='git status'
#Open a session of tmux
alias tmxs='tmux attach -t'
#shortcut for aliase
alias aliaserc='vim ~/.bash_aliases'
alias pychar='sh /opt/pycharm-community-2020.2.3/bin/pycharm.sh > /dev/null 2>&1 &'
alias pyrobot='python3.9 -m robot'
# function to create a directory and move into it.
mkcd () {
    mkdir -pv $1
    cd $1
}
