#!/bin/bash

setxkbmap us altgr-intl -option caps:swapescape &
picom &
~/.fehbg &
dwmblocks &
